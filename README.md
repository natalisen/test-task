Test Task with Stencil 

Natalja Sentjureva

Task description:

To build a web app using StencilJS compiler (https://stenciljs.com/);

The app should interact with any public third-party API;

It could be a crypto-currency parser for example, but it is up to you
(You can pick something related).

The app should consist of at least 3 screens;

1. The login screen. It should check credentials to be equal to
"Username: Test; Password: password";
2. Home screen should be a list of entities: here you can display a
list of crypto-currencies (or something related);
3. Entity view. When on you pick an entity, you should see details of
the entity on a separate screen;


npm run