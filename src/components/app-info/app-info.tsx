import {Component, Prop, State } from '@stencil/core';

@Component({
  tag: 'app-info',
  styleUrl: 'app-info.css'
})
export class AppInfo {
   @Prop() history: RouterHistory;
   @State() data: any;

  render() {
    return (
      <div class='app-info'>
        <div class='page'>
       <div class='container'>
    <div class='left'>
      <div class='sidebar-text'>Bitcoin rate to <span>{this.history.location.state.data.code}</span></div>
    </div>
    <div class='right'>
         <div class="rate-info">
        <p>{this.history.location.state.data.description}</p>
        <p>{this.history.location.state.data.rate_float}</p>
        </div>
       <a class="btn-back" onClick={() => this.history.push('/profile/currency',  {})}>Back
        </a>
    </div>
  </div>
  </div>
</div>
    );
  }
}