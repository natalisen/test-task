import { Component, Prop } from '@stencil/core';
import { MatchResults, RouterHistory } from '@stencil/router';


@Component({
  tag: 'app-profile',
  styleUrl: 'app-profile.css'
})


export class AppProfile {
  
  @Prop() match: MatchResults;
    @State() data: any;
    @Prop() history: RouterHistory;
  
  componentWillLoad() {
  
  return fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
    .then(response => response.json())
    .then(data => {
      this.data = data;
    });
    }
    
 showInfo(data) {
        this.history.push('/info/course', {data});
        
    }

  render() {
    if (this.match && this.match.params.name) {
      return (
        <div class='app-profile'>
        <div class='page'>
       <div class='container'>
    <div class='left'>
      <div class='sidebar-text'>Choose currency</div>
    </div>
    <div class='right'>
        <ul class="currency-list">
           {Object.keys(this.data.bpi).map(currency=><button onClick={() => { this.showInfo(this.data.bpi[currency]); }}><li>{currency}</li></button>)
            }
            
        </ul>
    </div>
  </div>
  </div> 
        </div>
      );
    }
  }
}
