import {Component, Element, EventEmitter, Event, Listen, Prop} from '@stencil/core';
import { RouterHistory } from '@stencil/router';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

@State() value: string;

    @Event() loginShouldOccur: EventEmitter;
    @Element() host: HTMLElement;
    @Prop() history: RouterHistory;

    login() {
        this.host.querySelector('p').innerHTML = "";
        this.host.querySelector('p').style.visibility = "none";
        
        
        let inputs = this.host.querySelectorAll('input');
              let user = {
                username: inputs[0].value,
                password: inputs[1].value
              };
        if (user.username == "Test" && user.password == "password"){
                
                this.history.push('/profile/currency', {});
                
        }else if(user.username == "" && user.password == ""){
        this.host.querySelector('p').innerHTML = "Please fill the form!";
            this.host.querySelector('p').style.visibility = "visible";
            
        }else{
            this.host.querySelector('p').innerHTML = "Your username or password is not valid!";
            this.host.querySelector('p').style.visibility = "visible";

        }
        
    }
    
    @Listen('keydown.enter')
    handleEnter() {
        this.login();
    }

  render() {
    return (
      <div class='app-home'>
        <div class='page'>
       <div class='container'>
    <div class='left'>
      <div class='login'>Login</div>
    </div>
    <div class='right'>
      <div class='form'>
        <label for='username'>Username</label>
        <input type='username' class='username' required/>
        <label for='password'>Password</label>
        <input type='password' class='password' required />
        <p class="error"></p>
        
         <button type="button" id="submit" value="Submit" onClick={() => { this.login(); }}>Submit</button>
        
      </div>
    </div>
  </div>
  </div>
</div>
    );
  }
}

  